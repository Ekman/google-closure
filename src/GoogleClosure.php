<?php

namespace NEkman\GoogleClosure;

use Guzzle\Http\Client;
use Guzzle\Http\EntityBody;

/**
 * https://developers.google.com/closure/compiler/docs/api-ref
 * @author Niklas Ekman <niklas.ekman@codr.se>
 */
class GoogleClosure {

	const URL = 'http://closure-compiler.appspot.com/compile';
	const OPTIMIZATION_SIMPLE = 'SIMPLE_OPTIMIZATIONS';
	const OPTIMIZATION_ADVANCED = 'ADVANCED_OPTIMIZATIONS';
	const OPTIMIZATION_WHITESPACE_ONLY = 'WHITESPACE_ONLY';
	const LIMIT_SIZE = 200000; // bytes

	private $opt;

	/**
	 * 
	 * @param OPTIMIZATION_* $opt Optimization levels. See the OPTIMIZATION_* constants
	 * @throws InvalidArgumentException
	 */
	public function __construct($opt = self::OPTIMIZATION_SIMPLE) {
		if ($opt !== self::OPTIMIZATION_SIMPLE &&
			$opt !== self::OPTIMIZATION_ADVANCED &&
			$opt !== self::OPTIMIZATION_WHITESPACE_ONLY) :
			throw new InvalidArgumentException('$opt must be of one of the OPTIMIZATION_* constants');
		endif;

		$this->opt = $opt;
	}

	/**
	 * Minify a collection of files
	 * @param \NEkman\GoogleClosure\FileCollection $files
	 * @return string $files minified
	 */
	public function minify(FileCollection $files) {
		if (sizeof($files) <= 0) :
			return '';
		endif;

		$content = '';
		$process = new FileCollection;

		$limit = self::LIMIT_SIZE * 0.9;

		foreach (new FileCollectionIterator($files) as $file) :
			$fileSize = filesize($file);

			// Check that this file can be fitted in the next response
			if ($process->size() + $fileSize > $limit) :
				// It cannot, start processing
				$content .= $this->process($process);
				$process->reset();
			endif;

			$process[] = $file;
		endforeach;

		if ($process->count() > 0) :
			$content .= $this->process($process);
		endif;

		return $content;
	}

	/**
	 * Do the actual minifying using Googles API
	 * @param \NEkman\GoogleClosure\FileCollection $files
	 * @return String Minified JavaScript of $files
	 * @throws Exception\GoogleClosureException
	 */
	private function process(FileCollection $files) {
		$unprocessedContent = '';
		foreach (new FileCollectionIterator($files) as $file) :
			// IMPORTANT NEWLINE!
			$unprocessedContent .= file_get_contents($file) . PHP_EOL;
		endforeach;

		$http = new Client;
		$request = $http->post(self::URL, null, [
		    'js_code' => $unprocessedContent,
		    'output_format' => 'json',
		    'output_info' => 'compiled_code'
		]);

		$response = $request->send();
		if ($response->getStatusCode() !== 200) :
			throw new GoogleClosureException("Closure error (#{$response->getStatusCode()}): {$response->getBody(true)}");
		endif;

		$body = $response->getBody();
		$data = self::body($body);

		if (array_key_exists('serverErrors', $data)) :
			$errors = json_encode($data['serverErrors']);
			throw new GoogleClosureException("Server errors: $errors");
		endif;

		return $data['compiledCode'];
	}

	private static function body(EntityBody $body) {
		$json = (string) $body;
		return json_decode($json, true);
	}

}

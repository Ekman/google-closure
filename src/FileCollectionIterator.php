<?php

namespace NEkman\GoogleClosure;

/**
 * Iterate over a FileCollection
 * @see FileCollection
 * @author Niklas Ekman <niklas.ekman@codr.se>
 */
class FileCollectionIterator implements \Iterator {

	private $files;
	private $index = 0;

	public function __construct(FileCollection $files) {
		$this->files = $files;
	}

	public function current() {
		return $this->files->get($this->index);
	}

	public function key() {
		return $this->index;
	}

	public function next() {
		$this->index++;
	}

	public function rewind() {
		$this->index = 0;
	}

	public function valid() {
		return $this->files->has($this->index);
	}

}

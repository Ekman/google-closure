<?php

namespace NEkman\GoogleClosure;

/**
 * Collection ensuring that all elements are valid files
 * @author Niklas Ekman <niklas.ekman@codr.se>
 */
class FileCollection implements \ArrayAccess {

	private $files = [];
	private $size = 0;
	private $count = 0;

	/**
	 * Add a file to the collection
	 * @param string $file Path to a valid readable file
	 * @throws InvalidArgumentException
	 */
	public function add($file) {
		if (is_dir($file) || !file_exists($file) || !is_readable($file)) :
			throw new InvalidArgumentException("$file is not a valid readable file");
		endif;

		$this->files[] = $file;
		$this->size += filesize($file);
		$this->count++;
	}

	/**
	 * Add multiple files
	 * @param array $files
	 */
	public function addArray(array $files) {
		foreach ($files as $file) :
			$this->add($file);
		endforeach;
	}

	/**
	 * Remove a file from the collection
	 * @param int $index Index
	 */
	public function remove($index) {
		if ($this->has($index)) :
			$this->size -= filesize($this->files[$index]);
			unset($this->files[$index]);
			$this->count--;
			// Fix indexes!
			$this->files = array_values($this->files);
		endif;
	}

	/**
	 * Check if the collection has the specified index
	 * @param int $index
	 * @return boolean true/false
	 */
	public function has($index) {
		return array_key_exists($index, $this->files);
	}

	/**
	 * Get a file by its index
	 * @param int $index
	 * @return string Path to a file
	 * @throws \InvalidArgumentException
	 */
	public function get($index) {
		if ($this->has($index)) :
			return $this->files[$index];
		endif;

		throw new \InvalidArgumentException("$index does not exist in the collection");
	}

	/**
	 * Reset the collection
	 */
	public function reset() {
		$this->files = [];
		$this->size = $this->count = 0;
	}

	/**
	 * Total size of all the files in the collection
	 * @return int Size of all the files in bytes
	 */
	public function size() {
		return $this->size;
	}

	/**
	 * The amount of elements in the collection
	 * @return int
	 */
	public function count() {
		return $this->count;
	}

	/**
	 * Sort the files given a callback function
	 * @param \Closure $sort
	 */
	public function sort(\Closure $sort) {
		usort($this->files, $sort);
	}
	
	/**
	 * Create an iterator for this collection
	 * @return \NEkman\GoogleClosure\FileCollectionIterator
	 */
	public function iterator() {
		return new FileCollectionIterator($this);
	}

	public function offsetExists($offset) {
		return $this->has($offset);
	}

	public function offsetGet($offset) {
		return $this->get($offset);
	}

	public function offsetSet($offset, $value) {
		$this->add($value);
	}

	public function offsetUnset($offset) {
		$this->remove($offset);
	}

}
